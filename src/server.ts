import config from 'config';
import Telegram  from './lib/telegram';
import Esploit  from './lib/esploit';
import telegramCommandProcessing  from './lib/telegramCommandProcessing';
const esploiterConfig = config.get<EsploiterConfig>('esploiter');
const main = (async () => {
  const esploit = new Esploit(esploiterConfig.esploitHost, esploiterConfig.esploitPort,
   esploiterConfig.esploitUsername, esploiterConfig.esploitPassword);
  const telegram = new Telegram(esploiterConfig.telegramToken, esploiterConfig.telegramId,
    (_telegram: Telegram) => telegramCommandProcessing(_telegram, esploit)
  );
  await telegram.sendMessage('esploiter is online');
  console.log('esploiter is online');
});
main();
