interface EsploiterConfig {
  telegramToken: string;
  telegramId: string | number;
  esploitUsername: string;
  esploitPassword: string;
  esploitHost: string;
  esploitPort: number;
}
