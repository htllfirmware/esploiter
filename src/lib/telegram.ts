import TelegramBot from 'node-telegram-bot-api';
interface Dialig {
    type: string;
    args?: any;
}
export default class Telegram {
  bot: TelegramBot;
  telegramId: number | string;
  commandProcessor: (telegram: Telegram) => void;
  dialog: Dialig;
  /**
   * telegram lib
   * @param telegramToken notification bot telegram token
   * @param telegramId    telegram user id for notifications
   */
  constructor(telegramToken: string, telegramId: number | string, commandProcessor:  (bot: Telegram) => void) {
    this.bot = new TelegramBot(telegramToken, {
      polling: true,
    });
    this.dialog = {
      type: 'none'
    };
    this.telegramId = telegramId;
    commandProcessor(this);
  }
  /**
   * send telegram message
   * @param  message message's text
   */
  async sendMessage(message: string): Promise<void> {
    await this.bot.sendMessage(this.telegramId, message);
  }
}
