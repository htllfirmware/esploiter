import rp from 'request-promise';
import unduck from './unduck';

export default class Esploit {
  host: string;
  port: number;
  username: string;
  password: string;
  constructor(host: string, port: number, username: string, password: string) {
    this.host = host;
    this.port = port;
    this.username = username;
    this.password = password;
  }
  async ping() {
    await rp(`http://${this.host}:${this.port}`);
  }
  async getPayloadList(): Promise<string> {
    let payloadsPage: string;
    try {
      payloadsPage = await rp(`http://${this.host}:${this.port}/listpayloads`);
    } catch (e) {
      throw new Error('offline');
    }
    let payloadPaths = payloadsPage.match(/\/showpayload\?payload=\/payloads\/(.+?)"/g);
    let payloads = [];
    for (const path of payloadPaths) {
      payloads.push(/\/payloads\/(.+?)"/g.exec(path)[1]);
    }
    return payloads.join('\r\n');
  }
  async runPayload(name: string): Promise<void> {
      await rp(`http://${this.host}:${this.port}/dopayload?payload=/payloads/${name}`);
  }
  async delPayload(name: string): Promise<void> {
      const options = {
        method: 'GET',
        uri: `http://${this.host}:${this.port}/deletepayload/yes?payload=/payloads/${name}`,
        headers: {
            'Authorization': 'Basic ' + Buffer.from(`${this.username}:${this.password}`).toString('base64')
        }
      };
      await rp(options);
  }
  async uploadPayload(duckPayload: string, name: string) {
    const unducked = unduck(duckPayload);
    const options = {
      method: 'POST',
      uri: `http://${this.host}:${this.port}/upload`,
      headers: {
          'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryeiAkO3BAbtfYuMjt'
      },
      body: '------WebKitFormBoundaryeiAkO3BAbtfYuMjt\r\n' +
      'Content-Disposition: form-data; name="upload"; filename="' + name + '"\r\n' +
      'Content-Type: text/plain\r\n\r\n' +
      unducked +
      '\r\n\r\n------WebKitFormBoundaryeiAkO3BAbtfYuMjt--\r\n\r\n'
    };
    await rp(options);
  }
}
