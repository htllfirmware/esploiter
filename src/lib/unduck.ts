const commandMap = {
  ESCAPE: '177',
  ESC: '177',
  GUI: '131',
  WINDOWS: '131',
  COMMAND: '131',
  MENU: '229',
  APP: '229',
  END: '213',
  SPACE: '32',
  BACKSPACE: '178',
  TAB: '179',
  PRINTSCREEN: '206',
  ENTER: '176',
  RETURN: '176',
  UPARROW: '218',
  DOWNARROW: '217',
  LEFTARROW: '216',
  RIGHTARROW: '215',
  UP: '218',
  DOWN: '217',
  LEFT: '216',
  RIGHT: '215',
  CAPSLOCK: '193',
  INSERT: '209',
  DELETE: '212',
  DEL: '212',
  F1: '194',
  F2: '195',
  F3: '196',
  F4: '197',
  F5: '198',
  F6: '199',
  F7: '200',
  F8: '201',
  F9: '202',
  F10: '203',
  F11: '204',
  F12: '205',
  PAGEUP: '211',
  PAGEDOWN: '214'
};
const comboMap = {
  ALT: '130',
  SHIFT: '129',
  CTRL: '128',
  CONTROL: '128',
  'CTRL-ALT': '128+130',
  'CTRL-SHIFT': '128+129'
};
const keyMap = {
  a: '97',
  b: '98',
  c: '99',
  d: '100',
  e: '101',
  f: '102',
  g: '103',
  h: '104',
  i: '105',
  j: '106',
  k: '107',
  l: '108',
  m: '109',
  n: '110',
  o: '111',
  p: '112',
  q: '113',
  r: '114',
  s: '115',
  t: '116',
  u: '117',
  v: '118',
  w: '119',
  x: '120',
  y: '121',
  z: '122'
};

export default function  unduck(toParse: string): string {
  let parsedScript = '';
  let lastLines;
  let lastCount;
  let parsedOut = '';
  let commandKnown = false;
  let noNewline = false;
  let wordArray;
  let wordOne;
  toParse = toParse.replace(/</g, '&lt;');
  toParse = toParse.replace(/^ +| +$/gm, '');
  toParse = toParse.replace(/\t/g, '');
  let lineArray = toParse.split('\n');
  for (let i = 0; i < lineArray.length; i++) {
    if (lineArray[i] === '' || lineArray[i] === '\n') {
      continue;
    }
    if (parsedOut !== undefined && parsedOut !== '') {
      lastLines = parsedOut;
      lastCount = ((lastLines.split('\n')).length + 1);
    }
    parsedOut = '';
    commandKnown = false;
    noNewline = false;
    wordArray = lineArray[i].split(' ');
    wordOne = wordArray[0];
    switch (wordOne) {
      case 'STRING':
        wordArray.shift();
        let textString = wordArray.join(' ');
        textString = textString.split('\\').join('\\\\').split('"').join('\\"');
        if (textString !== '') {
          parsedOut = 'Print:' + textString;
          commandKnown = true;
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', STRING needs a text');
        }
        break;
      case 'DELAY':
        wordArray.shift();
        if (wordArray[0] === undefined || wordArray[0] === '') {
          throw new Error('Error: at line: ' + (i + 1) + ', DELAY needs a time');
        }
        if (wordArray[0]) {
          parsedOut = 'CustomDelay:' + wordArray[0];
          commandKnown = true;
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', DELAY only accepts numbers');
        }
        break;
      case 'PINBRUTE':
        wordArray.shift();
        if (wordArray[0] === undefined || wordArray[0] === '') {
          throw new Error('Error: at line: ' + (i + 1) + ', PINBRUTE needs a number');
        }
        if (wordArray[0]) {
          let pinbruteCurrentTry = 1;
          let pinbruteMaxTry = parseInt(wordArray[1], 10);
          let pinbruteCurrent = 0;
          let pinbruteEnd = parseInt(Array(parseInt(wordArray[0], 10) + 1).join('9'), 10);
          while (pinbruteCurrent <= pinbruteEnd) {
            parsedOut += 'PrintLine:' +
             Array(Math.max(parseInt(wordArray[0], 10) - String(pinbruteCurrent).length + 1, 0)).join('0') +
             pinbruteCurrent + '\n';
            if (pinbruteCurrentTry === pinbruteMaxTry && pinbruteCurrent < pinbruteEnd) {
              parsedOut += 'CustomDelay:' + wordArray[2] + '\n';
              pinbruteCurrentTry = 0;
            }
            pinbruteCurrentTry++;
            pinbruteCurrent++;
          }
          commandKnown = true;
          noNewline = true;
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', PINBRUTE only accepts numbers');
        }
        break;
      case 'DEFAULTDELAY':
      case 'DEFAULT_DELAY':
        wordArray.shift();
        if (wordArray[0] === undefined || wordArray[0] === '') {
          throw new Error('Error: at line: ' + (i + 1) + ', DEFAULT_DELAY needs a time');
        }
        if (wordArray[0]) {
          parsedOut = 'DefaultDelay:' + wordArray[0];
          commandKnown = true;
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', DEFAULT_DELAY only accepts numbers');
        }
        break;
      case 'TYPE':
        wordArray.shift();
        if (wordArray[0] === undefined || wordArray[0] === '') {
          throw new Error('Error: at line: ' + (i + 1) + ', TYPE needs a key');
        }
        if ((<any>keyMap)[parseInt(wordArray[0], 10)]) {
          commandKnown = true;
          parsedOut = 'Press:' + (<any>keyMap)[wordArray[0]];
        } else {
          throw new Error('Error: Unknown letter \'' + wordArray[0] + '\' at line: ' + (i + 1));
        }
        break;
      case 'REM':
        wordArray.shift();
        if (wordArray.length > 0) {
          commandKnown = true;
          parsedOut = 'Rem:' + wordArray.join(' ');
          if (i === (lineArray.length - 1)) {
            parsedOut += '\n';
          }
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', REM needs a comment');
        }
        break;
      case 'REPEAT':
      case 'REPLAY':
        wordArray.shift();
        if (wordArray[0] === undefined || wordArray[0] === '') {
          throw new Error('Error: at line: ' + (i + 1) + ', REPEAT/REPLAY needs a loop count');
        }
        if (lastLines === undefined) {
          throw new Error('Error: at line: ' + (i + 1) + ', nothing to repeat, this is the first line.');
        }
        if (wordArray[0]) {
          let linesTmp = parsedScript.split('\n');
          linesTmp.splice(-lastCount, lastCount);
          if (linesTmp.join('\n') === '') {
            parsedScript = linesTmp.join('\n');
          } else {
            parsedScript = linesTmp.join('\n') + '\n';
          }
          lastLines = lastLines.replace(/^  /gm, '    ');
          parsedOut = lastLines + '\n';
          while (parseInt(wordArray[0], 10) > 1) {
            parsedOut += lastLines + '\n';
            wordArray[0] = String(parseInt(wordArray[0], 10) - 1);
          }
          parsedOut += lastLines;
          commandKnown = true;
        } else {
          throw new Error('Error: at line: ' + (i + 1) + ', REPEAT/REPLAY only accepts numbers');
        }
        break;
      default:
        if (wordArray.length === 1) {
          if ((<any>comboMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>comboMap)[wordArray[0]];
          } else if ((<any>commandMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>commandMap)[wordArray[0]];
          } else if ((<any>keyMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>keyMap)[wordArray[0]];
          } else {
            commandKnown = false;
            break;
          }
          wordArray.shift();
        }
        if (wordArray.length > 1) {
          if ((<any>comboMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>comboMap)[wordArray[0]] + '\+';
          } else if ((<any>commandMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>commandMap)[wordArray[0]] + '\+';
          } else if ((<any>keyMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += 'Press:' + (<any>keyMap)[wordArray[0]] + '\+';
          } else {
            commandKnown = false;
            break;
          }
          wordArray.shift();
        }
        while (wordArray.length > 1) {
          if ((<any>comboMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>comboMap)[wordArray[0]] + '\+';
          } else if ((<any>commandMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>commandMap)[wordArray[0]] + '\+';
          } else if ((<any>keyMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>keyMap)[wordArray[0]] + '\+';
          } else {
            commandKnown = false;
            break;
          }
          wordArray.shift();
        }
       while (wordArray.length === 1) {
          if ((<any>comboMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>comboMap)[wordArray[0]];
          } else if ((<any>commandMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>commandMap)[wordArray[0]];
          } else if ((<any>keyMap)[wordArray[0]] !== undefined) {
            commandKnown = true;
            parsedOut += (<any>keyMap)[wordArray[0]];
          } else {
            commandKnown = false;
            break;
          }
          wordArray.shift();
        }
    }
    if (!commandKnown) {
      throw new Error('Error: Unknown command or key \'' + wordArray[0] + '\' at line: ' + (i + 1) + '.');
    }
    parsedScript += parsedOut;
    if (!noNewline) {
      parsedScript += '\n';
    }
  }
  return parsedScript;
}
