import Telegram from './telegram';
import TelegramBot from 'node-telegram-bot-api';
import Esploit  from './esploit';

async function ping(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  await telegram.bot.sendMessage(chatId, 'yo');
}

async function pingDev(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    await esploit.ping();
    await telegram.bot.sendMessage(chatId, 'online');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function list(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    const payloadList = await esploit.getPayloadList();
    await telegram.bot.sendMessage(chatId, payloadList);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function initPayloadAdding(telegram: Telegram, msg: TelegramBot.Message, name: string) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    telegram.dialog = {
      type: 'add',
      args: name
    };
    await telegram.bot.sendMessage(chatId, 'send some Ducky code');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}

async function addPayload(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    const name = telegram.dialog.args;
    telegram.dialog = {
      type: 'none'
    };
    await esploit.uploadPayload(msg.text, name);
    await telegram.bot.sendMessage(chatId, 'payload added');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function del(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message, name: string) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    await esploit.delPayload(name);
    await telegram.bot.sendMessage(chatId, 'payload removed');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function run(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message, name: string) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    await esploit.runPayload(name);
    await telegram.bot.sendMessage(chatId, 'payload evaluated');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function messageProcessing(telegram: Telegram, esploit: Esploit, msg: TelegramBot.Message) {
    if (telegram.dialog.type === 'add') {
      await addPayload(telegram, esploit, msg);
    }
}

export default function telegramCommandProcessing(telegram: Telegram, esploit: Esploit): void {
  telegram.bot.onText(/^\/ping$/, (msg) => ping(telegram, msg));
  telegram.bot.onText(/^\/pingDev$/, (msg) => pingDev(telegram, esploit, msg));
  telegram.bot.onText(/^\/list$/, (msg) => list(telegram, esploit, msg));
  telegram.bot.onText(/^\/add (\w+)$/, (msg, matches) => initPayloadAdding(telegram, msg, matches[1]));
  telegram.bot.onText(/^\/del (\w+)$/, (msg, matches) => del(telegram, esploit, msg, matches[1]));
  telegram.bot.onText(/^\/run (\w+)$/, (msg, matches) => run(telegram, esploit, msg, matches[1]));
  telegram.bot.on('message', (msg) => messageProcessing(telegram, esploit, msg));
}
